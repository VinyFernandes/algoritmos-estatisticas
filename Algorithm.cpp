/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Algorithm.cpp
 * Author: vinicius
 * 
 * Created on 14 de Setembro de 2018, 08:41
 */

#include "Algorithm.h"

/**
 * 
 */
Algorithm::Algorithm() {
}

/**
 * 
 */
Algorithm::~Algorithm() {
}

/**
 * 
 * @param array
 * @return 
 */
vector<double> Algorithm::moda(vector<double> array) {

    vector<double> moda;
    map<double, int> count;
    double maior = 0;

    for (vector<double>::iterator it = array.begin(); it != array.end(); it++) {

        double num = (*it);
        if (count.count(num) <= 0)  // Se não existe , inseri no map
            count.insert(pair<double, int>(num, 0));

        int total = (count.find(num)->second) + 1;
        count.find(num)->second = total;
        maior = (maior < total) ? total : maior;
    }

    for (std::map<double, int>::iterator it = count.begin(); it != count.end(); ++it) {
        if (it->second == maior)
            moda.push_back(it->first);
    }

    return moda;
}

/**
 * 
 * @param array
 * @return 
 */
double Algorithm::mediana(vector<double> array) {

    double mediana = 0;
    sort(array.begin(), array.end());

    switch (array.size() % 2) {

        case 0: // Faixa de valores (qtd de elem do vetor) PAR.
            mediana = ((array.at(array.size() / 2 - 1)) + (array.at(array.size() / 2))) / 2;
            break;

        case 1: // Faixa de valores do vetor IMPAR.
            mediana = array.at(array.size() / 2);
            break;
    }
    return mediana;
}

/**
 * 
 * @param array
 * @return 
 */
double Algorithm::mediaGeometrica(vector<double> array) {

    double mult = 1;

    for (vector<double>::iterator it = array.begin(); it != array.end(); it++) {
        mult *= (*it);
    }
    return this->enesimaRaiz(mult, array.size());
}

/**
 * 
 * @param array
 * @return 
 */
double Algorithm::mediaAritmetica(vector<double> array) {

    double soma = 0;

    for (vector<double>::iterator it = array.begin(); it != array.end(); it++) {
        soma += (*it);
    }
    return (soma / array.size());
}

/**
 * 
 * @param array
 * @return 
 */
double Algorithm::desvioPadrao(vector<double> array) {

    double variancia = this->variancia(array);
    double desvioPadrao = sqrt(variancia);

    return desvioPadrao;
}

/**
 * 
 * @param map
 * @return 
 */
double Algorithm::mediaPonderada(map<double, int> map) {

    double somaValues = 0;
    double somaPesos = 0;

    for (std::map<double, int>::iterator it = map.begin(); it != map.end(); ++it) {
        somaValues += (it->first * it->second);
        somaPesos += it->second;
    }
    return (somaValues / somaPesos);
}

/**
 * 
 * @param valor
 * @param raiz
 * @return 
 */
double Algorithm::enesimaRaiz(double valor, int raiz) {

    if (valor >= 0) {

        switch (raiz) {
            case 1:
                return valor;
            case 2:
                return sqrt(valor);
            case 3:
                return cbrt(valor);
            default:
                return pow(valor, (1.0 / raiz));
        }
    }
    return -1;
}

/**
 * 
 * @param array
 * @return 
 */
double Algorithm::variancia(vector<double> array) {

    double mediaAritmetica = this->mediaAritmetica(array);
    vector<double> diferenca;

    for (vector<double>::iterator it = array.begin(); it != array.end(); it++) {
        diferenca.push_back(pow(((*it) - mediaAritmetica), 2));
    }
    return this->mediaAritmetica(diferenca);
}

